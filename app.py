from flask import Flask, render_template, request, jsonify
import threading
import time
import serial

app = Flask(__name__)

# Global variables to manage sensor readings and system status
system_initialized = False
monitoring = False
sensor_data = []

# Configure the serial port to which Arduino is connected
# Replace '/dev/ttyACM0' with the appropriate port for your setup
arduino_port = '/dev/ttyACM0'
baud_rate = 9600
ser = serial.Serial(arduino_port, baud_rate, timeout=1)

# Function to read data from Arduino
def read_from_arduino():
    global sensor_data, monitoring
    while monitoring:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            light_level, distance = line.split(',')
            light_level = int(light_level)
            distance = float(distance)
            sensor_data.append((time.time(), light_level, distance))
        time.sleep(0.1)  # Reduce sleep interval to 0.1 seconds

# Route to render the main page with control buttons
@app.route('/')
def index():
    return render_template('index.html')

# Route to initialize the system
@app.route('/initialize', methods=['POST'])
def initialize():
    global system_initialized
    system_initialized = True
    return jsonify({"status": "System initialized"})

# Route to start monitoring
@app.route('/start', methods=['POST'])
def start():
    global monitoring, sensor_data
    if system_initialized:
        monitoring = True
        sensor_data = []  # Clear sensor data before starting
        threading.Thread(target=read_from_arduino).start()
        return jsonify({"status": "Monitoring started"})
    else:
        return jsonify({"status": "System not initialized"}), 400

# Route to stop monitoring
@app.route('/stop', methods=['POST'])
def stop():
    global monitoring
    monitoring = False
    return jsonify({"status": "Monitoring stopped"})

# Route to disconnect the system
@app.route('/disconnect', methods=['POST'])
def disconnect():
    global system_initialized, monitoring
    system_initialized = False
    monitoring = False
    ser.close()
    return jsonify({"status": "System disconnected"})

# Route to get sensor data
@app.route('/data')
def data():
    return jsonify(sensor_data)

# Route to clear sensor data
@app.route('/clear', methods=['POST'])
def clear():
    global sensor_data
    sensor_data = []
    return jsonify({"status": "Data cleared"})

# Route to clear light data in text file
@app.route('/clear_light', methods=['POST'])
def clear_light():
    lines = []
    with open('sensor_data.txt', 'r') as f:
        lines = f.readlines()
    
    with open('sensor_data.txt', 'w') as f:
        for line in lines:
            if 'Light' not in line:
                f.write(line)
                
    return jsonify({"status": "Light data cleared from txt"})

# Route to clear distance data in text file
@app.route('/clear_distance', methods=['POST'])
def clear_distance():
    lines = []
    with open('sensor_data.txt', 'r') as f:
        lines = f.readlines()
    
    with open('sensor_data.txt', 'w') as f:
        for line in lines:
            if 'Distance' not in line:
                f.write(line)
                
    return jsonify({"status": "Distance data cleared from txt"})

# Route to save sensor data to text file
@app.route('/save_data', methods=['POST'])
def save_data():
    global sensor_data
    with open('sensor_data.txt', 'w') as f:
        for entry in sensor_data:
            timestamp, light_level, distance = entry
            f.write(f"{timestamp},Light,{light_level}\n")
            f.write(f"{timestamp},Distance,{distance}\n")
    return jsonify({"status": "Data saved to txt"})

# Route to load light data from text file
@app.route('/load_light', methods=['GET'])
def load_light():
    light_data = []
    with open('sensor_data.txt', 'r') as f:
        for line in f:
            timestamp, data_type, value = line.strip().split(',')
            if data_type == 'Light':
                light_data.append((timestamp, value))
    return jsonify(light_data)

# Route to load distance data from text file
@app.route('/load_distance', methods=['GET'])
def load_distance():
    distance_data = []
    with open('sensor_data.txt', 'r') as f:
        for line in f:
            timestamp, data_type, value = line.strip().split(',')
            if data_type == 'Distance':
                distance_data.append((timestamp, value))
    return jsonify(distance_data)

# Run the Flask app
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5003)
